<?php session_start(); ?>

<html>

<head>
  <meta charset="utf-8">
  <title>MPBPred-Sequence-based TMP Binding-sites Predict Tool</title>
  <link rel="stylesheet" href="MPBPred.css">
</head>
<body>
<div id="container">

<div id="logo">

<table>
	<tr>
		<img src="Figs/mpbpred1.jpg">
	</tr>
</table>

</div>

<hr />
<div id="menu">

<ul>
<li><a href="MPBPred.php" id="current" >Home</a></li>
<li><a href="MPBPred_help.php">Help</a></li>
<li><a href="MPBPred_contact.php">Contact</a></li>
<li><a  href="MPBPred_download.php">Download</a></li>
</ul>


</div>

<hr />
<div id="main">
<h2>Contact</h2>
<p>This server is maintaned by Bo Zeng (zeng(at)wzw.tum.de in case of any problems please contact him or for feedback please contact him.<br>
The webserver is hosted by the Wissenshafts Zentrum Weihenstephan in the lab of <a href="http://frishman.wzw.tum.de/">Dmitrij Frishman</a>.
</p>
</div>
</body>
</html>
