#!/home/students/zeng/miniconda3/bin/python3
import sys
import subprocess,threading
import os.path
import shutil
import smtplib
import time


def getQuedJobs():
    out = []
    qFiles = os.listdir("/home/students/zeng/workspace/test2/MPBPred_jobs/q")
    for qfile in qFiles:
        qFIleFH = open("/home/students/zeng/workspace/test2/MPBPred_jobs/q/%s" % qfile)
        out.append(qFIleFH.readline())
        qFIleFH.close()
    return out

def main():
    while os.listdir("/home/students/zeng/workspace/test2/MPBPred_jobs/q/")==[]:
        time.sleep(1)
    perl_loc="/home/students/zeng/usr/local/bin/perl"
    perl_test_file="/home/students/zeng/TMBPRED/TMBPRED/TMBPredTest1.pl"
    #setting_file="/home/students/zeng/PycharmProjects/repos/sithipy/sithipy/setting/sithipy_run_setting_linux.xlsx"

    #for row in getQuedJobs():
    row=getQuedJobs()[0]
    jobID ,email = row.split("\t")
    fasta_input_file=os.path.join("/home/students/zeng/workspace/test2/MPBPred_out","%s/QuePro.fasta") %jobID
    class_file="/home/students/zeng/workspace/test2/MPBPred_class/AllFit.rf"
    output_file=os.path.join("/home/students/zeng/workspace/test2/MPBPred_out","%s/output.csv") %jobID
    exect_str="{perl} {perl_test} -i {input} -c {rfclass} -o {output} -e {email_address}".format(perl=perl_loc, perl_test=perl_test_file, input=fasta_input_file, rfclass=class_file, output=output_file,email_address=email)
    os.system("mv /home/students/zeng/workspace/test2/MPBPred_jobs/q/%s /home/students/zeng/workspace/test2/MPBPred_jobs/r/%s" % (jobID, jobID))
    
    class Command(object):
        def __init__(self, cmd):
            self.cmd = cmd
            self.process = None
   
        def run(self, timeout):
            def target():
                print('Thread started')
                self.process = subprocess.Popen(self.cmd,shell=True,stdout=subprocess.PIPE)
                subprocess.call(self.cmd,shell=True)
                print('Thread finished')
   
            thread = threading.Thread(target=target)
            thread.start()
   
            thread.join(timeout)
            if thread.is_alive():
                print('Terminating process')
                self.process.terminate()
                thread.join()
            print(self.process.returncode)
   
    command = Command(exect_str)
    command.run(timeout=2000)                       
    os.system("mv /home/students/zeng/workspace/test2/MPBPred_jobs/r/%s /home/students/zeng/workspace/test2/MPBPred_jobs/f/%s" % (jobID, jobID))

        

if __name__ == "__main__":
        try:
                main()
        except KeyboardInterrupt:
                pass
