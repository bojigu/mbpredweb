<?php session_start(); ?>


<html>
<head>
	<meta http-equiv="refresh" content="10">
	<title>MPBPred-Sequence-based TMP Binding-sites Predict Tool</title>
  <link rel="stylesheet" href="../../MPBPred.css">
</head>
<body>
<div id="container">

<div id="logo">
<table>
	<tr>
		<img src="../../Figs/mpbpred1.jpg">
	</tr>
</table>
</div>

<hr />
<div id="menu">

<ul>
<li><a href="../../MPBPred.php" id="current" >Home</a></li>
<li><a href="../../MPBPred_help.php">Help</a></li>
<li><a href="../../MPBPred_contact.php">Contact</a></li>
<li><a  href="../../MPBPred_download.php">Download</a></li>
</ul>

</div>

<hr />
<div id="main">
<?php
	
	function checkInput($fname){
		if(!file_exists($fname)){
			echo "<p>The server failed to recieve any information.<br>Please check your input.<br>If this problem continues please contact zeng@wzw.tum.de.<p>";
			return False;
		}
		if(count(file($fname))<1){
			echo "<p>The server failed to recieve any information.<br>Please check your input.<br>If this problem continues please contact zeng@wzw.tum.de.<p>";
			return False;
		}

		$file = fopen($fname, "r") or exit("Unable to open file!");
		$line_number = 1;
		$fasta_number=0;
		$formatError = False;
		$header= fgets($file);
		$fasta_seq="";
		if(!preg_match("/^>/",$header)){
			echo "please make sure your input protein sequence is in fasta format";
			$formatError=True;
		}
		while(!feof($file)) {
			$line = fgets($file);
			if($line == ""){
				continue;
			}
			if(preg_match("/^>/", $line)){
				$fasta_number+=1;
				if($fasta_number>=2){
					echo "you have inputted more than one fasta files,please input one fasta file once";
					$formatErro=True;
					return False;
				}
			}
			if(!preg_match("/^>/", $line)){
				$fasta_seq .=$line;
			}
		}
		$fasta_seq=str_replace(array("\n","\r\n","\r","\s","\t"),"",$fasta_seq);
		if(strlen($fasta_seq)<30){
			echo "Sorry,your input sequence is less than 30aa. Please input again!";
			return False;
		}
		if(strlen($fasta_seq)>5000){
			echo "Sorry,your input sequence is longer than 5000aa. Please input again!";
			return False;
		}
		if($formatError){
			echo "Input must contain fasta header and protein sequence, and no more than one fasta sequence at one time <br>";
			return False;
		}
		return True;
	}
	
	$fields = explode( "/", $_SERVER['SCRIPT_FILENAME']);
	$id = $fields[7];
	echo $id;
	echo "\n";
	$inputFile = "../../MPBPred_out/$id/QuePro.fasta";
	if(checkInput($inputFile) == False){
		
	}elseif(file_exists("../../MPBPred_out/$id/output.csv")){
		echo "<h2>The job ($id) has finished</h2><p>Your results are stored up to two week.<br>Download the data <a href=\"output.csv\">here</a><br></p>";
		$content = file_get_contents("output.csv");
		$content = explode("\n", $content);
		$delIDs = "";
		$i = 0;
		$table =  "<table border=\"1\">";
		foreach($content as $line){
			$line = $content = str_replace(",", "</td><td>" , $line);
			if($line !=""){
				$table .= "<tr><td>$line</td></tr>";
			}
		}
		$table .= "</table>";
		echo $table;
	}else{
		$id = $_SESSION['id'];
		echo "<p>Your job with the ID:$id is currently running and may take several minutes to finish.<br>When completed,the result will be loaded on this page also send to your email.<p>";
	}
?>
</div>
</body>
</html>
