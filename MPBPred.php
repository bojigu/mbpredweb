<?php
ob_start();
session_start(); ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>


<head>
<link rel="stylesheet" type="text/css" href="MPBPred.css"/>

<title>MPBPred-Sequence-based TMP Binding-sites Predict Tool</title>
<meta http-equiv="Content-Type" content="text/html"; charset="UTF-8"">

<script type="text/javascript">
	function clearText(field){
	    if (field.defaultValue == field.value) field.value = '';
	    else if (field.value == '') field.value = field.defaultValue;
	}

	function example1(){
		document.forms["MPBPred"]["fastaText"].value = ">3B9W:A|PDBID|CHAIN|SEQUENCE\nSAVAPAEINEARLVAQYNYSINILAMLLVGFGFLMVFVRRYGFSATTGTYLVVATGLPLYILLRANGIFGHALTPHSVDA\nVIYAEFAVATGLIAMGAVLGRLRVFQYALLALFIVPVYLLNEWLVLDNASGLTEGFQDSAGSIAIHAFGAYFGLGVSIAL\nTTAAQRAQPIESDATSDRFSMLGSMVLWLFWPSFATAIVPFEQMPQTIVNTLLALCGATLATYFLSALFHKGKASIVDMA\nNAALAGGVAIGSVCNIVGPVGAFVIGLLGGAISVVGFVFIQPMLESKAKTIDTCGVHNLHGLPGLLGGFSAILIVPGIAV\nAQLTGIGITLALALIGGVIAGALIKLTGTTKQAYEDSHEFIHLAGPEDEHKAERLVLEAKTEIQGLKNRIDAAVLSAKSE\nGHHHHHH";
	}

</script>

<script type="text/javascript">
 function show(id){
        document.getElementById(id).style.display='block';
    }
    function hide(id){
        document.getElementById(id).style.display='none';
    }
</script>



</head>

<body>
<?php

        function checkInput($text){
                $out = "";
                $line_number = 1;
		$lines = explode("\n", $text);
                foreach($lines as $line) {
			if($line == ""){
				continue;
			}
                        if(!preg_match("/^[A-Za-z0-9~`!#$%^&*()_+-]+[\s\t][A-Za-z0-9~`!#$%^&*()_+-]+[\n\s]?$/", $line)){
                                $out = "<span class=\"error\">Error in parsing input at line $line_number. Line: \"$line\" </span>";
                        }
			$line_number = $line_number + 1;
                }
                return $out;
        }


if ($_SERVER["REQUEST_METHOD"] == "POST"){
	// Make ID
	$id = -1;
	$err = 0;
	try{
		$id = getUniqID();
	}catch(Exception $e){
		$err = 1;
		echo 'Message: ' .$e->getMessage();
	}
	$_SESSION['id'] = $id;
	mkdir("./MPBPred_out/$id", $mode = 0777);
	@chmod("./MPBPred_out/$id", 0777);
	// Create Value for user inputs

	if(isset($_POST["email"])){
		$_SESSION{'email'}=$_POST["email"];
	}
	
	if(!(isset($_POST["fastaText"]) && $_POST["fastaText"] != "") && !(is_uploaded_file($_FILES['fastaFile']['tmp_name']))){
		$textErr = "<span class=\"error\"> Please add fasta sequence in the text field or supply a file for upload.</span>";
		$err = 1;
	}
	if((isset($_POST["fastaText"]) && $_POST["fastaText"] != "") && (is_uploaded_file($_FILES['fastaFile']['tmp_name']))){
                $textErr = "<span class=\"error\"> You have both uploaded a file, and entered fasta sequence. Please provide only one entry point.</span>";
                $err = 1;
        }
	// Check text
	if(isset($_POST["fastaText"]) && $_POST["fastaText"] != ""){
		$fastaText = $_POST["fastaText"];
		$lines = count(explode("\n", $fastaText));
		if($lines<=1000){
			file_put_contents("./MPBPred_out/$id/QuePro.fasta", $fastaText);
			@chmod("./MPBPred_out/$id/QuePro.fasta", 0777);
		}else{
			$textErr = "<span class=\"error\"> Textfield has more than 1000 lines (one interaction per line)</span>";
			$err = 1;
		}
		$validateText = checkInput($fastaText);
		if($validateText != ""){
			$textErr = $validateText;
			$err = 1;
		}
	}
	// Check File
	if(is_uploaded_file($_FILES['fastaFile']['tmp_name'])){
		if ($_FILES["fastaFile"]["size"] < 1048576){
			if(wc_l($_FILES['fastaFile']['tmp_name']) == 0){
				$fileErr = "<span class=\"error\">Selected file is empty, please try again!</span>";
				$err = 1;
			}elseif(wc_l($_FILES['fastaFile']['tmp_name']) <= 1000 && wc_l($_FILES['fastaFile']['tmp_name']) > 0){
				$fileText = file_get_contents($_FILES['fastaFile']['tmp_name']);
				$validateText = checkInput($fileText);
				if($validateText != ""){
					$fileErr = $validateText;
					$err = 1;
				}
				if(move_uploaded_file($_FILES['fastaFile']['tmp_name'], "./MPBPred_out/$id/QuePro.fasta")){
					@chmod("./MPBPred_out/$id/QuePro.fasta", 0777);
				} else {
			    		 $fileErr = "<span class=\"error\">There was an error uploading the file, please try again!</span>";
					$err = 1;
				}
			
			}else{
				$fileErr =  "<span class=\"error\">*File has too many lines (max 1000)</span>";
				$err = 1;
			}
		}else{
			$fileErr =  "<span class=\"error\"> File size is too large (max 1MB)</span>";
			$err = 1;
		}
	}
	header("Location: MPBPred_submit.php");
	die();
}

function wc_l($file){
$linecount = 0;
$handle = fopen($file, "r");
while(!feof($handle)){
  $line = fgets($handle, 4096);
  $linecount = $linecount + substr_count($line, PHP_EOL);
}

fclose($handle);

return $linecount;
}

function getUniqID(){
	$i = 0;
	$uniqID = 0;
	while($i < 1000){
		$i++;
		$uniqID = uniqid();
		$filename = "./MPBPred_out/$uniqID";
		if (!file_exists($filename)) {
			return $uniqID;
		}
	}
	throw new Exception("Can not create random uniqID");
	return null;
}

function test_input($data)
{
     $data = stripslashes($data);
     $data = htmlspecialchars($data);
     return $data;
}
?>


<div id="container">

<div id="logo">

<table>
	<tr>
		<td>
			<img border="0" src="Figs/mpbpred1.jpg">
		</td>
	</tr>
</table>

</div>

<hr />
<div id="menu">

<ul>
<li><a href="MPBPred.php" id="current" >Home</a></li>
<li><a href="MPBPred_help.php">Help</a></li>
<li><a href="MPBPred_contact.php">Contact</a></li>
<li><a  href="MPBPred_download.php">Download</a></li>
</ul>

</div>

<hr />


<div id="content">
<h3>Welcome to MPBPred!</h3>
<p>MPBPred (Membrane Protein Binding-sites Prediciton) is a web server developed for the binding sites prediction of trans-membrane proteins.</p>
<form name = "MPBPred"  method="post" action="<?php echo $_SERVER["PHP_SELF"]?>" enctype="multipart/form-data"> 
   <br>
   Please paste TM protein full sequence below in fasta format (<a href = "javascript:example1()" >Example</a>)
   <br>
   <?php echo $textErr;?> <br><textarea name="fastaText" id="fastaText" rows="10" cols="80"></textarea>
   <br>
   or upload a file: 
   <input type="file" name="fastaFile"> <?php echo $fileErr;?>
	<br><br>
	Enter your Email address, where the prediciton results will be sent to:
	<br>
   Email: <input name="email" type="text" /><br />
   <br>
   
   <input type="submit" name="submit" value="Submit"/> 
   <br><br>

</form>

<p>If you wish to cite MPBPred, please refer to our publication:</p>
<p>Zeng,B., Hoenigschmid,P., Frishman,D. Alpha-helical membrane protein interface residues prediciton using a sequence-based random forest approach. </p>


</div>

<div id="footer">
<table>
<tr>
<th><h4>Copyright&copy;Frishman's Lab, &nbsp;All rights reserved<h4></th>
</tr>
</table>
</div>


</div>

</body>
</html>

