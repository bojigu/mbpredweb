<?php session_start(); ?>

<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
  <title>MPBPred-Sequence-based TMP Binding-sites Predict Tool</title>
  <link rel="stylesheet" href="MPBPred.css">

</head>
<body>
<div id="container">

<div id="logo">

<table>
	<tr>
		<img src="Figs/mpbpred1.jpg">
	</tr>
</table>

</div>

<hr />
<div id="menu">

<ul>
<li><a href="MPBPred.php" id="current" >Home</a></li>
<li><a href="MPBPred_help.php">Help</a></li>
<li><a href="MPBPred_contact.php">Contact</a></li>
<li><a  href="MPBPred_download.php">Download</a></li>
</ul>


</div>

<hr />
<div id="main">
<h2>About</h2>
<p>Here we present A Random Forest binary classifier called TMBPred (transmembrane

protein binding sites prediction), an accurate 3D structure-independent computational method

for classifying TMP residues into binding or non-binding. We applied features of evolutionary

rate, PSSM, co-evolutionary strength, relative distance, and physical parameters into the

classifier. We find that the conservation is higher in binding residues than non-binding residues,

and interacting residues have preferable binding location in the transmembrane region or in the full protein sequence, 

also binding residues have relative higher mean co-evolution strength.

</p>

<p>The Train DataSet of TMPs were extracted form multiple-pass membrane complexes in PDB database. Below display an TMP complex example got from protein with PDBID 3b9w.</p>

<li><a  href="pdb/3b9w.pdb">3b9w</a></li>
<div id="viewport" style="width:400px; height:400px;"></div>
<script src="ngl/dist/ngl.js"></script>
<script>
    document.addEventListener( "DOMContentLoaded", function() {
        var stage = new NGL.Stage( "viewport" );
        stage.loadFile( "pdb/3b9w.pdb", { defaultRepresentation: true } );
    } );
</script>

</div>
</body>
</html>
